package com.lrns123.abeoadmin.modules;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityTargetLivingEntityEvent;

import com.lrns123.abeoadmin.AbeoAdmin;

public class ToggleFlyCommand implements IModule, CommandExecutor, Listener{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(cmd.getName().equalsIgnoreCase("toggleflight")){
			Player player = (Player) sender;
			if(player.hasPermission("abeoadmin.fly")) {
				if(args.length == 0) {
					if(player.getAllowFlight() == true) {
						player.setAllowFlight(false);
						player.sendMessage(ChatColor.BLUE + "Your flight mode has been set to false");
					} else {
						player.setAllowFlight(true);
						player.sendMessage(ChatColor.BLUE + "Your flight mode has been set to true");
					}
				}
				if(args.length == 1) {
					Player target = Bukkit.getPlayerExact(args[0]);
					if(target == null || !target.isOnline()) {
						player.sendMessage(ChatColor.RED + args[0] + " is not online");
					} else {
						if(target.getAllowFlight() == true) {
							target.setAllowFlight(false);
							player.sendMessage(ChatColor.BLUE + "Flight mode has been set to false for " + target.getDisplayName());
							target.sendMessage(ChatColor.BLUE + "Your flight mode has been set to false");
							if(!target.hasPermission("abeoadmin.fly")) {
								target.setGameMode(GameMode.SURVIVAL);
							}
						} else {
							target.setAllowFlight(true);
							player.sendMessage(ChatColor.BLUE + "Flight mode has been set to true for " + target.getDisplayName());
							target.sendMessage(ChatColor.BLUE + "Your flight mode has been set to true");
							if(!target.hasPermission("abeoadmin.fly")) {
								target.setGameMode(GameMode.ADVENTURE);
							}
						}
					}
				}
				if(args.length >= 2) {
					player.sendMessage(ChatColor.RED + "Too many arguements");
				}
			} else {
				player.sendMessage(ChatColor.RED + "Insufficient permissions");
			}
			return true;
		}
		return false;
	}

	@Override
	public void registerModule(AbeoAdmin instance) {
		instance.getCommand("toggleflight").setExecutor(this);
		instance.getServer().getPluginManager().registerEvents(this, instance);
	}
	
	@EventHandler
	public void onMobTarget(EntityTargetLivingEntityEvent event) {
		if(event.getTarget() instanceof Player) {
			Player ply = (Player) event.getTarget();
			if(ply.getAllowFlight() == true) {
				event.setCancelled(true);
			}
		}
		
	}
	
	@EventHandler
	public void onSuffocate(EntityDamageEvent event) {
		if((event.getEntity() instanceof Player) && event.getCause().equals(DamageCause.SUFFOCATION)) {
			Player ply = (Player) event.getEntity();
			if(ply.getAllowFlight() == true) {
				event.setCancelled(true);
			}
		}
		
	}

}