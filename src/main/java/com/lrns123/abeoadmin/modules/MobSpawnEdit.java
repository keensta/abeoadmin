package com.lrns123.abeoadmin.modules;

import java.lang.reflect.Field;

import net.minecraft.server.v1_7_R1.TileEntityMobSpawner;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_7_R1.block.CraftCreatureSpawner;
import org.bukkit.entity.Player;

import com.lrns123.abeoadmin.AbeoAdmin;
import com.lrns123.abeoadmin.util.Mob;

public class MobSpawnEdit implements IModule, CommandExecutor {

	
	private TileEntityMobSpawner getSpawnerTileEntity(Block block) {
		if (block.getType() != Material.MOB_SPAWNER) {
			return null;
		} else {
			CraftCreatureSpawner spawner = (CraftCreatureSpawner)block.getState();
			try {
				Field field = spawner.getClass().getDeclaredField("spawner");
				field.setAccessible(true);
				TileEntityMobSpawner tileEntity = (TileEntityMobSpawner)field.get(spawner);
				return tileEntity;
			} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
				return null;
			}
		}
	}
	
	private boolean SetSpawnerEntity(TileEntityMobSpawner spawner, String mobName) {
		Field field;
		try {
			field = spawner.getClass().getDeclaredField("mobName");
			field.setAccessible(true);
			field.set(spawner, mobName);
			return true;
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			return false;
		}
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player))
			return true;

		if (!sender.hasPermission("abeoadmin.spawner"))
			return true;

		Player ply = (Player) sender;

		if (args.length == 0) {
			Block target = ply.getTargetBlock(null, 100);
			if (target != null && target.getType() == Material.MOB_SPAWNER) {
				CreatureSpawner spawner = (CreatureSpawner) target.getState();
				sender.sendMessage(ChatColor.BLUE + "Spawner mob: " + spawner.getSpawnedType().getName());
			} else {
				sender.sendMessage(ChatColor.RED + "You must be aiming at a mob spawner to use this command");
			}
		} else {
			Block target = ply.getTargetBlock(null, 100);
			if (target != null && target.getType() == Material.MOB_SPAWNER) {
				String mobId = args[0];
				Mob mob = Mob.fromName(mobId);
				if (mob == null) {
					sender.sendMessage(ChatColor.RED + "Invalid mob specified");
				} else {
					CreatureSpawner spawner = (CreatureSpawner) target.getState();
					try {					
						spawner.setSpawnedType(mob.getType());
					} catch (Exception e) {
						// Pre 1.3.2 workaround
						TileEntityMobSpawner spawnerTile = getSpawnerTileEntity(target);
						if (!SetSpawnerEntity(spawnerTile, mob.getType().getName())) {
							sender.sendMessage(ChatColor.RED + "Could not change spawner");
							return true;
						}
						
					}
					sender.sendMessage(ChatColor.BLUE + "Mob spawner modified");
				}
			} else {
				sender.sendMessage(ChatColor.RED + "You must be aiming at a mob spawner to use this command");
			}
		}
		return true;
	}

	@Override
	public void registerModule(AbeoAdmin instance) {
		instance.getCommand("spawner").setExecutor(this);
	}

}
