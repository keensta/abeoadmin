package com.lrns123.abeoadmin.modules;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import com.lrns123.abeoadmin.AbeoAdmin;

public class NetherCalcCommand implements IModule, CommandExecutor {


	@Override
	public void registerModule(AbeoAdmin instance) {
		instance.getCommand("nethercalc").setExecutor(this);
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage("Must be in-game to run this command.");
			return true;
		}
		Player player = (Player) sender;
		if(cmd.getName().equalsIgnoreCase("nethercalc")){
			if(!player.hasPermission("abeoadmin.nethercalc")) {
				player.sendMessage(ChatColor.RED + "Insufficient permissions.");
				return true;
			}
			Location loc = player.getLocation();
			int x = (int) loc.getX()/8;
			int y = (int) loc.getY();
			int z = (int) loc.getZ()/8;
			player.sendMessage(ChatColor.GRAY + "Make sure you are standing in the player's unlit portal.");
			player.sendMessage(ChatColor.GOLD + "The portal will go at: " + ChatColor.WHITE + x + ", " + y + ", " + z + ".");
			
		}
		return false;
	}

	
}
