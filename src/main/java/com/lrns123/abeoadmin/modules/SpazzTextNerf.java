package com.lrns123.abeoadmin.modules;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import com.lrns123.abeoadmin.AbeoAdmin;

public class SpazzTextNerf implements Listener, IModule {

	@Override
	public void registerModule(AbeoAdmin instance) {
		instance.getServer().getPluginManager().registerEvents(this, instance);
	}

	@EventHandler
	public void onPlayerChat(AsyncPlayerChatEvent ev) {
		// Block the &k color code
		ev.setMessage(ev.getMessage().replaceAll("&k", ""));

		// Block explicitly typed color codes
		ev.setMessage(ev.getMessage().replaceAll("(?i)\u00A7[0-9A-FK]", ""));
		
		// Fix formatting codes
		if (ev.getPlayer().hasPermission("chatmanager.chat.color")) {
			ev.setMessage(ev.getMessage().replaceAll("(?i)&([LMNOR])", "\u00A7$1"));
		}
	}
}
