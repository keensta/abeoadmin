package com.lrns123.abeoadmin.modules;

import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityChangeBlockEvent;

import com.lrns123.abeoadmin.AbeoAdmin;

public class EnderNerf implements Listener, IModule {

	@Override
	public void registerModule(AbeoAdmin instance) {
		instance.getServer().getPluginManager().registerEvents(this, instance);

	}

	@EventHandler
	public void onEndermanEvent(EntityChangeBlockEvent ev) {
		if (ev.getEntityType() == EntityType.ENDERMAN)
			ev.setCancelled(true);
	}
}
