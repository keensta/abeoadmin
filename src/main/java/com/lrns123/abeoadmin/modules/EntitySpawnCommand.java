package com.lrns123.abeoadmin.modules;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import com.lrns123.abeoadmin.AbeoAdmin;

public class EntitySpawnCommand implements CommandExecutor, IModule {

	
	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		
		if(!(sender instanceof Player))
			return true;
		
		Player p = (Player) sender;
		
		if(p.hasPermission("abeoadmin.entityspawn")) {
			if(args.length == 0) {
				p.sendMessage(ChatColor.RED + "Usage: '/entityspawn entity' ");
				return true;
			}
			EntityType et = EntityType.fromName(args[0]);
			
			if(et == null) {
				p.sendMessage(ChatColor.RED + "This Entity Does not exist!");
				return true;
			}
			
			
			p.getWorld().spawnEntity(p.getLocation(), et);
		} else {
			p.sendMessage(ChatColor.RED + "Not enought permission to use this!");
		}
		
		return true;
	}
	
	
	@Override
	public void registerModule(AbeoAdmin instance) {
		instance.getCommand("entityspawn").setExecutor(this);
	}
	
}
