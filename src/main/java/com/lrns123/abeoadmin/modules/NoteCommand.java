package com.lrns123.abeoadmin.modules;

import java.util.logging.Logger;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import com.lrns123.abeoadmin.AbeoAdmin;

public class NoteCommand implements IModule, CommandExecutor {

	Logger log = Logger.getLogger("Minecraft");

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if (!sender.hasPermission("abeoadmin.lognote"))
			return true;
		StringBuilder sb = new StringBuilder();
		for (String arg : args) {
			sb.append(arg);
			sb.append(" ");
		}
		log.fine("Log Note by " + sender.getName() + ": " + sb.toString());
		sender.sendMessage(ChatColor.BLUE + "Log notice added");
		return true;
	}

	@Override
	public void registerModule(AbeoAdmin instance) {
		instance.getCommand("lognote").setExecutor(this);
	}

}
