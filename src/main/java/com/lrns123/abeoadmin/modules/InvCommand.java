package com.lrns123.abeoadmin.modules;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;

import com.lrns123.abeoadmin.AbeoAdmin;

public class InvCommand implements IModule, CommandExecutor  {
	

	@Override
	public void registerModule(AbeoAdmin instance) {
		instance.getCommand("inv").setExecutor(this);
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,String[] args) {
		Player player = (Player) sender;
		if(cmd.getName().equalsIgnoreCase("inv")){
			if(player.hasPermission("abeoadmin.inv")){
				if(args.length == 0) {
					player.sendMessage(ChatColor.BLUE + "Usage: To clear your own inventory /inv clear");
					player.sendMessage(ChatColor.BLUE + "Usage: To clear target's inventory /inv clear [playername]");
					player.sendMessage(ChatColor.BLUE + "Usage: To clear target chest /inv clear -i (Item in Hand)");
					player.sendMessage(ChatColor.BLUE + "Usage: To clear target chest /inv clear -c (Look at Chest)");
					player.sendMessage(ChatColor.BLUE + "Usage: To view target's inventory /inv see [playername]");
					player.sendMessage(ChatColor.BLUE + "Usage: To view target's EnderChest /inv see [playername] -ec");
				}
				if(args.length == 1) {
					if(args[0].equalsIgnoreCase("clear")) {
						player.getInventory().clear();
						player.sendMessage(ChatColor.GOLD + "Inventory is now clear.");
					}
					if(args[0].equalsIgnoreCase("see")) {
						player.sendMessage(ChatColor.RED + "Usage: To view target's inventory /inv see [playername]");
					}
				}
				if(args.length >= 2) {
					if(args[0].equalsIgnoreCase("clear")) {
						Player target = Bukkit.getPlayerExact(args[1]);				
						if (target == null || !target.isOnline()) {
							if(args[1].equalsIgnoreCase("-c")) {
								Block btarget = player.getTargetBlock(null, 100);
								if (btarget.getState() instanceof InventoryHolder) {
									Inventory inventory = ((InventoryHolder) btarget.getState()).getInventory();
									inventory.clear();
									player.sendMessage(ChatColor.GOLD + "Chest has been cleared");
								} else {
									player.sendMessage(ChatColor.RED + "You need to aim at a chest for this to work");
								}
								return true;
							}
							if(args[1].equalsIgnoreCase("-i")) {
								player.setItemInHand(null);
								player.sendMessage(ChatColor.GOLD + "Item in hand has been cleared");
								return true;
							}
							
							player.sendMessage(ChatColor.RED + "Invalid target specified");
						} else {
							target.getInventory().clear();
							target.sendMessage(ChatColor.RED + "Your Inventory just got cleared by a Mod/Admin");
							player.sendMessage(ChatColor.GOLD + target.getName() + " inventory has been cleared.");
						}
					}
					if(args[0].equalsIgnoreCase("see")) {
						Player target = Bukkit.getPlayerExact(args[1]);	
						if (target == null || !target.isOnline()) {
							player.sendMessage(ChatColor.RED + "Invalid target specified");
						} else {
							if(args.length == 3) {
								if(args[2].equalsIgnoreCase("-ec")) {
									player.openInventory(target.getEnderChest());
									return true;
								}
							}
							player.openInventory(target.getInventory());
						}
					}	
				}
/*				if(args.length >= 3) {
					player.sendMessage(ChatColor.BLUE + "Usage: To clear your own inventory /inv clear");
					player.sendMessage(ChatColor.BLUE + "Usage: To clear target's inventory /inv clear [playername]");
					player.sendMessage(ChatColor.BLUE + "Usage: To clear target chest /inv clear -i (Item in Hand)");
					player.sendMessage(ChatColor.BLUE + "Usage: To clear target chest /inv clear -c (Look at Chest)");
					player.sendMessage(ChatColor.BLUE + "Usage: To view target's inventory /inv see [playername]");
				}*/
			} else {
				player.sendMessage(ChatColor.RED + "Insufficient permissions");
			}
		}
		return false;
	}

}


