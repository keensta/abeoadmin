package com.lrns123.abeoadmin.modules;

import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.lrns123.abeoadmin.AbeoAdmin;

public class SignEdit implements CommandExecutor, IModule {

	@Override
	public void registerModule(AbeoAdmin instance) {
		instance.getCommand("signedit").setExecutor(this);

	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!sender.hasPermission("abeoadmin.signedit"))
			return false;

		if (sender instanceof Player) {
			Player ply = (Player) sender;
			Block target = ply.getTargetBlock(null, 100);
			if (target.getState() instanceof Sign) {
				int line;
				try {
					line = new Integer(args[0]);
				} catch (Exception e) {
					line = 0;
				}

				if (line < 1 || line > 4) {
					sender.sendMessage(ChatColor.RED + "Invalid line specified");
					return true;
				}
				StringBuilder msg = new StringBuilder();
				for (int i = 1; i < args.length; i++) {
					if (i != 1)
						msg.append(" ");
					msg.append(args[i]);
				}

				// Process colors
				String newText = msg.toString().replaceAll("(?i)&([0-9A-FKLMNOR])", "\u00A7$1");

				Sign sign = (Sign) target.getState();
				sign.setLine(line - 1, newText);
				sign.update();
				return true;
			} else {
				sender.sendMessage(ChatColor.RED + "You must be aiming at a sign to use this command");
				return true;
			}
		} else {
			sender.sendMessage("SignEdit cannot be used from the server console");
			return true;
		}
	}

}
