package com.lrns123.abeoadmin.modules;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.lrns123.abeoadmin.AbeoAdmin;
import com.lrns123.abeoadmin.util.Util;

/**
 * Handles the /enchant command
 * 
 * @author Lourens Elzinga
 * 
 */
public class EnchantCommand implements CommandExecutor, IModule {

	static private HashMap<String, Enchantment> enchantmentNames = new HashMap<String, Enchantment>();
	static private HashMap<Enchantment, String> enchantmentFriendlyNames = new HashMap<Enchantment, String>();

	// Name table used for enchantment lookups
	static {
		// Armor enchantments
		enchantmentNames.put("protection", Enchantment.PROTECTION_ENVIRONMENTAL);
		enchantmentNames.put("fireprotection", Enchantment.PROTECTION_FIRE);
		enchantmentNames.put("featherfalling", Enchantment.PROTECTION_FALL);
		enchantmentNames.put("blastprotection", Enchantment.PROTECTION_EXPLOSIONS);
		enchantmentNames.put("projectileprotection", Enchantment.PROTECTION_PROJECTILE);
		enchantmentNames.put("respiration", Enchantment.OXYGEN);
		enchantmentNames.put("aquaaffinity", Enchantment.WATER_WORKER);

		// Sword enchantments
		enchantmentNames.put("sharpness", Enchantment.DAMAGE_ALL);
		enchantmentNames.put("smite", Enchantment.DAMAGE_UNDEAD);
		enchantmentNames.put("baneofarthropods", Enchantment.DAMAGE_ARTHROPODS);
		enchantmentNames.put("knockback", Enchantment.KNOCKBACK);
		enchantmentNames.put("fireaspect", Enchantment.FIRE_ASPECT);
		enchantmentNames.put("looting", Enchantment.LOOT_BONUS_MOBS);

		// Bow enchantments
		enchantmentNames.put("power", Enchantment.ARROW_DAMAGE);
		enchantmentNames.put("punch", Enchantment.ARROW_KNOCKBACK);
		enchantmentNames.put("flame", Enchantment.ARROW_FIRE);
		enchantmentNames.put("infinity", Enchantment.ARROW_INFINITE);

		// Tool enchantments
		enchantmentNames.put("efficiency", Enchantment.DIG_SPEED);
		enchantmentNames.put("unbreaking", Enchantment.DURABILITY);
		enchantmentNames.put("silktouch", Enchantment.SILK_TOUCH);
		enchantmentNames.put("fortune", Enchantment.LOOT_BONUS_BLOCKS);
	}

	static {
		// Armor enchantments
		enchantmentFriendlyNames.put(Enchantment.PROTECTION_ENVIRONMENTAL, "Protection");
		enchantmentFriendlyNames.put(Enchantment.PROTECTION_FIRE, "Fire Protection");
		enchantmentFriendlyNames.put(Enchantment.PROTECTION_FALL, "Feather Falling");
		enchantmentFriendlyNames.put(Enchantment.PROTECTION_EXPLOSIONS, "Blast Protection");
		enchantmentFriendlyNames.put(Enchantment.PROTECTION_PROJECTILE, "Projectile Protection");
		enchantmentFriendlyNames.put(Enchantment.OXYGEN, "Respiration");
		enchantmentFriendlyNames.put(Enchantment.WATER_WORKER, "Aqua Affinity");

		// Sword enchantments
		enchantmentFriendlyNames.put(Enchantment.DAMAGE_ALL, "Sharpness");
		enchantmentFriendlyNames.put(Enchantment.DAMAGE_UNDEAD, "Smite");
		enchantmentFriendlyNames.put(Enchantment.DAMAGE_ARTHROPODS, "Bane of Arthropods");
		enchantmentFriendlyNames.put(Enchantment.KNOCKBACK, "Knockback");
		enchantmentFriendlyNames.put(Enchantment.FIRE_ASPECT, "Fire Aspect");
		enchantmentFriendlyNames.put(Enchantment.LOOT_BONUS_MOBS, "Looting");

		// Bow enchantments
		enchantmentFriendlyNames.put(Enchantment.ARROW_DAMAGE, "Power");
		enchantmentFriendlyNames.put(Enchantment.ARROW_KNOCKBACK, "Punch");
		enchantmentFriendlyNames.put(Enchantment.ARROW_FIRE, "Flame");
		enchantmentFriendlyNames.put(Enchantment.ARROW_INFINITE, "Infinity");

		// Tool enchantments
		enchantmentFriendlyNames.put(Enchantment.DIG_SPEED, "Efficiency");
		enchantmentFriendlyNames.put(Enchantment.DURABILITY, "Unbreaking");
		enchantmentFriendlyNames.put(Enchantment.SILK_TOUCH, "Silk Touch");
		enchantmentFriendlyNames.put(Enchantment.LOOT_BONUS_BLOCKS, "Fortune");

	}

	@Override
	public void registerModule(AbeoAdmin instance) {
		instance.getCommand("enchant").setExecutor(this);
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if (sender instanceof Player) {
			Player ply = (Player) sender;

			if (!ply.hasPermission("abeoadmin.enchant")) {
				// Pretend the command does not exist
				return false;
			}

			if (args.length < 1) {
				ply.sendMessage("Syntax: /enchant <add/remove/force/list> [enchantment] [level]");
				return true;
			}

			ItemStack item = ply.getItemInHand();
			if (item == null) {
				ply.sendMessage(ChatColor.RED + "You must have an item in your hand to use this command");
				return true;
			}

			if (args.length == 1) {
				if (args[0].equalsIgnoreCase("list")) {
					// List enchantments
					Map<Enchantment, Integer> enchantments = item.getEnchantments();

					sender.sendMessage(ChatColor.BLUE + "Enchantments on held item:");
					for (Enchantment ench : enchantments.keySet()) {
						sender.sendMessage(ChatColor.BLUE + (enchantmentFriendlyNames.containsKey(ench) ? enchantmentFriendlyNames.get(ench) : ench.getName())
								+ " " + enchantments.get(ench));
					}
					return true;
				} else {
					return true;
				}
			}

			Enchantment ench = null;
			if (Util.isInteger(args[1])) {
				ench = Enchantment.getById(Util.toInteger(args[1], -1));
			} else {
				ench = enchantmentNames.get(args[1].toLowerCase());
			}

			if (ench == null) {
				ply.sendMessage(ChatColor.RED + "Invalid enchantment specified");
				return true;
			}

			if (args[0].equalsIgnoreCase("add")) {

				if (args.length < 3) {
					ply.sendMessage(ChatColor.RED + "No enchantment level specified");
					return true;
				}

				int level = Util.toInteger(args[2], 0);

				if (level < 1) {
					ply.sendMessage(ChatColor.RED + "Invalid level specified");
					return true;
				}

				try {
					item.addEnchantment(ench, level);
					ply.setItemInHand(item);
					sender.sendMessage(ChatColor.BLUE + "Enchantment "
							+ (enchantmentFriendlyNames.containsKey(ench) ? enchantmentFriendlyNames.get(ench) : ench.getName()) + " " + level + " added");

				} catch (Exception e) {
					ply.sendMessage(ChatColor.RED + "Illegal enchantment specified");
				}

			} else if (args[0].equalsIgnoreCase("remove")) {

				int oldlvl = item.removeEnchantment(ench);
				ply.setItemInHand(item);
				if (oldlvl == 0) {
					sender.sendMessage(ChatColor.RED + "Enchantment was not present on item");
				} else {
					sender.sendMessage(ChatColor.BLUE + "Enchantment "
							+ (enchantmentFriendlyNames.containsKey(ench) ? enchantmentFriendlyNames.get(ench) : ench.getName()) + " " + oldlvl + " removed");
				}

			} else if (args[0].equalsIgnoreCase("force")) {
				if (args.length < 3) {
					ply.sendMessage(ChatColor.RED + "No enchantment level specified");
					return true;
				}

				int level = Util.toInteger(args[2], 0);

				if (level < 1) {
					ply.sendMessage(ChatColor.RED + "Invalid level specified");
					return true;
				}

				item.addUnsafeEnchantment(ench, level);
				ply.setItemInHand(item);
				sender.sendMessage(ChatColor.BLUE + "Enchantment "
						+ (enchantmentFriendlyNames.containsKey(ench) ? enchantmentFriendlyNames.get(ench) : ench.getName()) + " " + level + " added");

			} else {
				ply.sendMessage("Syntax: /enchant <add/remove/force> [enchantment] [level]");
			}
			return true;
		} else {
			// Ignore the command
			return true;
		}
	}
}
