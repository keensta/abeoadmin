package com.lrns123.abeoadmin.modules;

import org.bukkit.World.Environment;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;

import com.lrns123.abeoadmin.AbeoAdmin;

public class PigmenSubwayNerf implements IModule, Listener {

	@Override
	public void registerModule(AbeoAdmin instance) {
		instance.getServer().getPluginManager().registerEvents(this, instance);

	}

	@EventHandler
	public void onCreatureSpawn(CreatureSpawnEvent ev) {
		if (ev.getEntityType() == EntityType.PIG_ZOMBIE && ev.getSpawnReason() == SpawnReason.NATURAL && ev.getLocation().getBlockY() < 25
				&& ev.getLocation().getWorld().getEnvironment() == Environment.NETHER) {
			ev.setCancelled(true);
		}
	}

}
