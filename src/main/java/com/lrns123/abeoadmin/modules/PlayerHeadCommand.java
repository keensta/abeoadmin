package com.lrns123.abeoadmin.modules;


import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import com.lrns123.abeoadmin.AbeoAdmin;

public class PlayerHeadCommand implements CommandExecutor, IModule{
	
	public AbeoAdmin plugin = null;

	@Override
	public void registerModule(AbeoAdmin instance) {
		this.plugin = instance;
		
		plugin.getCommand("phead").setExecutor(this);
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(!(sender.hasPermission("abeoadmin.playerhead")))
			return true;
		if(!(sender instanceof Player))
			return true;
		
		Player ply = (Player) sender;
		if(args.length == 0) {
			ply.sendMessage(ChatColor.RED + "Not enought arguments. Use /phead <PlayerName>");
		} else {
			String target = args[0];
			if(ply.getItemInHand().getType() == Material.SKULL_ITEM) {
				ply.getItemInHand().setType(Material.AIR);
				ply.getInventory().addItem(setSkin(target));
				ply.sendMessage(ChatColor.GREEN + "You now have " + target + " skull.");
			} else { 
				ply.sendMessage(ChatColor.RED + "Can't find Skull, Make sure you are holding it!");
			}
		}
		return false;
	}
	
	
	public static ItemStack setSkin(String name) {
		ItemStack s = new ItemStack(Material.SKULL_ITEM);
		s.setDurability((short) 3);
		SkullMeta meta = (SkullMeta) s.getItemMeta();
		meta.setOwner(name);
		s.setItemMeta(meta);
		return s;
	}
}
