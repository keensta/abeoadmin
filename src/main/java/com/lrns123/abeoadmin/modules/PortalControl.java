package com.lrns123.abeoadmin.modules;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityCreatePortalEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.world.PortalCreateEvent;

import com.lrns123.abeoadmin.AbeoAdmin;

public class PortalControl implements IModule, Listener {

	private boolean blockPortal = false;

	@Override
	public void registerModule(AbeoAdmin instance) {
		instance.getServer().getPluginManager().registerEvents(this, instance);
	}

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		if (event.isCancelled())
			return;

		Player ply = event.getPlayer();
		if (event.getClickedBlock().getType() == Material.OBSIDIAN && event.getMaterial() == Material.FLINT_AND_STEEL) {
			if (!ply.hasPermission("abeoadmin.createportal")) {
				blockPortal = true;
			} else {
				blockPortal = false;
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPortalActivation(EntityCreatePortalEvent ev) {
		if (ev.getEntity() instanceof Player) {
			Player ply = (Player) ev.getEntity();
			if (!ply.hasPermission("abeoadmin.createportal")) {
				ply.sendMessage(ChatColor.RED + "You are not permitted to create a portal here. Please contact a moderator.");
				ev.setCancelled(true);
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void onPortalCreate(PortalCreateEvent ev) {
		if (blockPortal) {
			ev.setCancelled(true);
			blockPortal = false;
		}
	}
}
