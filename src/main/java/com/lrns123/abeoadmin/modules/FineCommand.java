package com.lrns123.abeoadmin.modules;

import net.milkbowl.vault.economy.Economy;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.lrns123.abeoadmin.AbeoAdmin;

public class FineCommand implements CommandExecutor, IModule{

	public AbeoAdmin plugin;
	
	@Override
	public void registerModule(AbeoAdmin instance) {
		this.plugin = instance;
		
		plugin.getCommand("fine").setExecutor(this);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(!(sender instanceof Player))
			return true;
		
		Player ply = (Player) sender;
		if(!(ply.hasPermission("abeoadmin.fine")))
			return true;
		
		if(args.length < 2) {
			sender.sendMessage(ChatColor.RED + "/fine [Player] [Precentage]");
		}
		
		if(args.length == 2) {
			OfflinePlayer target = (OfflinePlayer) Bukkit.getOfflinePlayer(args[0]);
			if(args[1].contains(".")) {
				ply.sendMessage(ChatColor.RED + "You can only use whole numbers for the percentage");
				return true;
			} else {
				int percentage = 100000000;		
				try {
					percentage = Integer.parseInt(args[1]);
				} catch (Exception e) {
					ply.sendMessage(ChatColor.RED + "The percentage has to be a number!!");
					ply.sendMessage(ChatColor.RED + "Usage: /fine [Player] [Percentage]");
				}
				if(percentage == 100000000) {
					ply.sendMessage(ChatColor.RED + "Error: 1");
					return true;
				}
				Economy eco = plugin.getEconomy();
				double percentageD = percentage;
				double ammount = eco.getBalance(target.getName());
				double fineAmount = ammount*(percentageD /100.0);
				eco.withdrawPlayer(target.getName(), fineAmount);
				ply.sendMessage(ChatColor.GREEN + "You have fined " + args[0] + " for " + fineAmount);
				if(target != null && target.isOnline()) {
					Player t = Bukkit.getPlayer(target.getName());
					t.sendMessage(ChatColor.RED + "You have been fined " + fineAmount + " by Staff");
				} 
			}
		}
 		
		return false;
	}



}
