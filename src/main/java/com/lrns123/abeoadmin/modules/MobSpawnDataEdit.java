package com.lrns123.abeoadmin.modules;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.lang.reflect.Field;

import net.minecraft.server.v1_7_R1.NBTCompressedStreamTools;
import net.minecraft.server.v1_7_R1.NBTTagCompound;
import net.minecraft.server.v1_7_R1.TileEntityMobSpawner;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_7_R1.block.CraftCreatureSpawner;
import org.bukkit.entity.Player;

import com.lrns123.abeoadmin.AbeoAdmin;

public class MobSpawnDataEdit implements CommandExecutor, IModule {

	private AbeoAdmin plugin; 
	
	@Override
	public void registerModule(AbeoAdmin instance) {
		plugin = instance;
		instance.getCommand("spawnerdata").setExecutor(this);
	}
	
	private TileEntityMobSpawner getSpawnerTileEntity(Block block) {
		if (block.getType() != Material.MOB_SPAWNER) {
			return null;
		} else {
			CraftCreatureSpawner spawner = (CraftCreatureSpawner)block.getState();
			try {
				Field field = spawner.getClass().getDeclaredField("spawner");
				field.setAccessible(true);
				TileEntityMobSpawner tileEntity = (TileEntityMobSpawner)field.get(spawner);
				return tileEntity;
			} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
				return null;
			}
		}
	}
	
	private NBTTagCompound getSpawnerData(TileEntityMobSpawner spawner) {
		Field field;
		try {
			field = spawner.getClass().getDeclaredField("spawnData");
			field.setAccessible(true);
			NBTTagCompound compound = (NBTTagCompound)field.get(spawner);
			return compound;
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			return null;
		}
	}
	
	private boolean setSpawnerData(TileEntityMobSpawner spawner, NBTTagCompound data) {
		Field field;
		try {
			field = spawner.getClass().getDeclaredField("spawnData");
			field.setAccessible(true);
			field.set(spawner, data);
			return true;
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			return false;
		}
	}
	
	private int getMinDelay(TileEntityMobSpawner spawner) {
		Field field;
		try {
			field = spawner.getClass().getDeclaredField("minSpawnDelay");
			field.setAccessible(true);
			int delay = (Integer)field.get(spawner);
			return delay;
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			return 0;
		}
	}
	
	private boolean setMinDelay(TileEntityMobSpawner spawner, int delay) {
		Field field;
		try {
			field = spawner.getClass().getDeclaredField("minSpawnDelay");
			field.setAccessible(true);
			field.set(spawner, delay);
			return true;
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			return false;
		}
	}
	
	private int getMaxDelay(TileEntityMobSpawner spawner) {
		Field field;
		try {
			field = spawner.getClass().getDeclaredField("maxSpawnDelay");
			field.setAccessible(true);
			int delay = (Integer)field.get(spawner);
			return delay;
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			return 0;
		}
	}
	
	private boolean setMaxDelay(TileEntityMobSpawner spawner, int delay) {
		Field field;
		try {
			field = spawner.getClass().getDeclaredField("maxSpawnDelay");
			field.setAccessible(true);
			field.set(spawner, delay);
			return true;
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			return false;
		}
	}
	
	private int getSpawnCount(TileEntityMobSpawner spawner) {
		Field field;
		try {
			field = spawner.getClass().getDeclaredField("spawnCount");
			field.setAccessible(true);
			int spawncount = (Integer)field.get(spawner);
			return spawncount;
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			return 0;
		}
	}
	
	private boolean setSpawnCount(TileEntityMobSpawner spawner, int spawncount) {
		Field field;
		try {
			field = spawner.getClass().getDeclaredField("spawnCount");
			field.setAccessible(true);
			field.set(spawner, spawncount);
			return true;
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			return false;
		}
	}
	
	private NBTTagCompound loadDataFromFile(String filename) {
		File f = new File(plugin.getDataFolder(), "spawnerdata/" + filename + ".nbt");
		if (f.exists()) {
			FileInputStream in;
			try {
				in = new FileInputStream(f);
				NBTTagCompound data = NBTCompressedStreamTools.a(in);
				return data;
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	private boolean saveDataToFile(String filename, NBTTagCompound data) {		
		File f = new File(plugin.getDataFolder(), "spawnerdata/" + filename + ".nbt");
		try {
			FileOutputStream out = new FileOutputStream(f);
			NBTCompressedStreamTools.a(data, out);
			return true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player))
			return true;

		if (!sender.hasPermission("abeoadmin.spawnerdata"))
			return true;

		Player ply = (Player) sender;


		Block target = ply.getTargetBlock(null, 100);
		if (target != null && target.getType() == Material.MOB_SPAWNER) {
			
			TileEntityMobSpawner spawner = getSpawnerTileEntity(target);
			if (spawner == null) {
				sender.sendMessage(ChatColor.RED + "Could not access tile entity data");
			} else {
				if (args.length == 0) {
					NBTTagCompound data = getSpawnerData(spawner);
					if (data == null) {
						sender.sendMessage(ChatColor.BLUE + "This spawner does not have custom data");
					} else {
						if (data.hasKey("SDName")) {
							sender.sendMessage(ChatColor.BLUE + "This spawner has custom data ('" + data.getString("SDName") + "')");
						} else {
							sender.sendMessage(ChatColor.BLUE + "This spawner has custom data");
						}
						
					}
				} else {
					if (args[0].equalsIgnoreCase("load")) {
						if (args.length < 2) {
							sender.sendMessage(ChatColor.RED + "Please specify the nbt file to load into the spawner");
						} else {
							
							if (args[1].contains("/") || args[1].contains("\\")) {
								sender.sendMessage(ChatColor.RED + "The name can not contain a / or \\");
							} else {
								NBTTagCompound data = loadDataFromFile(args[1]);
								if (data == null) {
									sender.sendMessage(ChatColor.RED + "Could not load the specified nbt file");
								} else {
									setSpawnerData(spawner, data);
									if (data.hasKey("SDName")) {
										sender.sendMessage(ChatColor.BLUE + "Spawner data has been set ('" + data.getString("SDName") + "')");
									} else {
										sender.sendMessage(ChatColor.BLUE + "Spawner data has been set");
									}
								}
							}
						}
					} else if (args[0].equalsIgnoreCase("save")) {
						if (args.length < 2) {
							sender.sendMessage(ChatColor.RED + "Please specify the nbt file to save the spawner data into");
						} else {
							NBTTagCompound data = getSpawnerData(spawner);
							
							if (data == null) {
								sender.sendMessage(ChatColor.RED + "The spawner does not have any custom data");
							} else if (args[1].contains("/") || args[1].contains("\\")) {
								sender.sendMessage(ChatColor.RED + "The name can not contain a / or \\");
							} else if (saveDataToFile(args[1], data)) {
								if (data.hasKey("SDName")) {
									sender.sendMessage(ChatColor.BLUE + "Spawner data has been saved ('" + data.getString("SDName") + "')");
								} else {
									sender.sendMessage(ChatColor.BLUE + "Spawner data has been saved");
								}
							} else {
								sender.sendMessage(ChatColor.RED + "Could not save spawner data");
							}
						}
					} else if (args[0].equalsIgnoreCase("clear")) {
						if (setSpawnerData(spawner, null)) {
							sender.sendMessage(ChatColor.BLUE + "Spawner data cleared");
						} else {
							sender.sendMessage(ChatColor.RED + " Could not clear spawner data");
						}
					} else if (args[0].equalsIgnoreCase("mindelay")) {
						if (args.length < 2) {
							int delay = getMinDelay(spawner);
							sender.sendMessage(ChatColor.BLUE + "Min Delay: " + delay + " ticks");
						} else {
							try {
								int delay = Integer.parseInt(args[1]);
								if (delay < 1)
									delay = 1;
								setMinDelay(spawner, delay);
								sender.sendMessage(ChatColor.BLUE + "Min delay set to " + delay + " ticks");
							} catch (Exception e) {
								sender.sendMessage(ChatColor.RED + "Invalid delay specified");
							}
						}
					} else if (args[0].equalsIgnoreCase("maxdelay")) {
						if (args.length < 2) {
							int delay = getMaxDelay(spawner);
							sender.sendMessage(ChatColor.BLUE + "Max Delay: " + delay + " ticks");
						} else {
							try {
								int delay = Integer.parseInt(args[1]);
								if (delay < 1)
									delay = 1;
								setMaxDelay(spawner, delay);
								sender.sendMessage(ChatColor.BLUE + "Max delay set to " + delay + " ticks");
							} catch (Exception e) {
								sender.sendMessage(ChatColor.RED + "Invalid delay specified");
							}
						}
					} else if (args[0].equalsIgnoreCase("spawncount")) {
						if (args.length < 2) {
							int spawncount = getSpawnCount(spawner);
							sender.sendMessage(ChatColor.BLUE + "Spawn Count: (up to) " + spawncount + " entities");
						} else {
							try {
								int spawncount = Integer.parseInt(args[1]);
								if (spawncount < 1)
									spawncount = 1;
								setSpawnCount(spawner, spawncount);
								sender.sendMessage(ChatColor.BLUE + "Spawn count set to (up to) " + spawncount + " entities");
							} catch (Exception e) {
								sender.sendMessage(ChatColor.RED + "Invalid count specified");
							}
						}
					}
				}
			}
		} else {
			sender.sendMessage(ChatColor.RED + "You must be aiming at a mob spawner to use this command");
		}
		return true;
	}

}
