package com.lrns123.abeoadmin.modules;

import java.io.File;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.util.Vector;

import com.lrns123.abeoadmin.AbeoAdmin;

public class ResetEndCommand implements IModule, Listener, CommandExecutor {

        @Override
        public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] arg3) {
                
                if(sender.hasPermission("abeoadmin.endreset")) {
                        
                        FileConfiguration cfg = YamlConfiguration.loadConfiguration(new File("plugins/AbeoMisc/", "warp.yml"));
                        Vector pos = getVector(cfg, "spawnpos");
                        String world = cfg.getString("spawnworld");
                        World eWorld = Bukkit.getWorld("world_the_end");
                        if (!cfg.contains("spawnpos") || !cfg.contains("spawnworld")) {
                                sender.sendMessage(ChatColor.RED + "Something has gone wrong!!");
                                return false;
                        }
                        
                        Location spawn = new Location(Bukkit.getWorld(world), pos.getX(), pos.getY(), pos.getZ());
                        
                        for (Player ply : Bukkit.getOnlinePlayers()) {
                                if(ply.getWorld() == eWorld) {
                                        ply.teleport(spawn);
                                }
                        }
                        sender.sendMessage(ChatColor.RED + "We will begin regenerating the End Realm in a moment, expect a small spike in performance...");
                        resetEnd(eWorld);
            sender.sendMessage(ChatColor.GREEN + "You have regenerated the End Realm successfully!");
                        return true;
                } else {
                        sender.sendMessage(ChatColor.RED + "Insufficient permission");
                }
                return false;
        }

        public void resetEnd(World world) {
            for (int x = -15; x <= 15; x++) {
                for (int z = -15; z <= 15; z++)
                {
                  if (!world.isChunkLoaded(x, z)) {
                    world.loadChunk(x, z);
                  }
                  try
                  {
                    if (!world.isChunkLoaded(x, z)) {
                      world.loadChunk(x, z);
                    }
                    
                    List<Entity> Entities = world.getEntities();
                    
                    if(!Entities.isEmpty()) {
                        for (Entity ent : Entities) {
                                if(ent != null) {
                                        ent.remove();
                                }
                        }
                    }
                    world.regenerateChunk(x, z);
                    Location dl = new Location(world, 0, 80, 0);
                    world.spawnEntity(dl, EntityType.ENDER_DRAGON);
                  }
                  catch (NullPointerException npe) {
                          npe.printStackTrace();
                  }
                }
              }
        }
        
        @Override
        public void registerModule(AbeoAdmin instance) {
                instance.getCommand("resetend").setExecutor(this);
        }
        
        private Vector getVector(FileConfiguration cfg, String path) {
                try {
                        List<Integer> list = cfg.getIntegerList(path);
                        if (list == null || list.size() != 3)
                                return new Vector();

                        return new Vector(list.get(0), list.get(1), list.get(2));
                } catch (Exception e) {
                        return new Vector();
                }
        }

}