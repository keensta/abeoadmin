package com.lrns123.abeoadmin.modules;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import com.lrns123.abeoadmin.AbeoAdmin;

public class UnequipCommand implements IModule, CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!sender.hasPermission("abeoadmin.unequip"))
			return true;
		
		if (args.length < 1) {
			sender.sendMessage(ChatColor.RED + "Please specify a target");
		} else {
			Player target = Bukkit.getPlayerExact(args[0]);
			if (target == null || target.isOnline() == false) {
				sender.sendMessage(ChatColor.RED + "Invalid target specified");
			} else {
				PlayerInventory inv = target.getInventory();

				ItemStack[] armor = inv.getArmorContents();
				HashMap<Integer, ItemStack> cantFit = inv.addItem(armor);
				
				inv.setHelmet(null);
				inv.setChestplate(null);
				inv.setLeggings(null);
				inv.setBoots(null);
				
				for (ItemStack item : cantFit.values()) {
					target.getWorld().dropItemNaturally(target.getLocation(), item);
				}
			}
		}
		return true;
	}

	@Override
	public void registerModule(AbeoAdmin instance) {
		instance.getCommand("unequip").setExecutor(this);
	}

}
