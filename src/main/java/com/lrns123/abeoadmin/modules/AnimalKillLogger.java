package com.lrns123.abeoadmin.modules;

import java.util.logging.Logger;

import org.bukkit.Location;
import org.bukkit.entity.Animals;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;

import com.lrns123.abeoadmin.AbeoAdmin;

public class AnimalKillLogger implements Listener, IModule {

	private Logger log = Logger.getLogger("Minecraft");

	@Override
	public void registerModule(AbeoAdmin instance) {
		instance.getServer().getPluginManager().registerEvents(this, instance);
	}

	@EventHandler(priority = EventPriority.MONITOR)
	void onEntityDeath(EntityDeathEvent ev) {
		Entity ent = ev.getEntity();

		// Animal death reporting
		if (ent instanceof Animals) {
			Player ply = ((Animals) ent).getKiller();
			Location loc = ent.getLocation();
			StringBuilder sb = new StringBuilder();

			sb.append("[AnimalKill] ");
			sb.append(ent.getClass().getSimpleName().replaceAll("Craft", ""));
			sb.append(" killed by ");
			sb.append(ply != null ? ply.getName() : "*Unknown*");
			sb.append(" at ");
			sb.append(loc.getX());
			sb.append(" ");
			sb.append(loc.getY());
			sb.append(" ");
			sb.append(loc.getZ());
			sb.append(" (");
			sb.append(loc.getWorld().getName());
			sb.append(")");

			log.info(sb.toString());
		}
	}
}
