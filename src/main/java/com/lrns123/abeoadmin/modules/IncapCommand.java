package com.lrns123.abeoadmin.modules;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.lrns123.abeoadmin.AbeoAdmin;

public class IncapCommand implements IModule, CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if (!sender.hasPermission("abeoadmin.incap"))
			return true;
		
		Player target;
		if (args.length < 1) {
			if (sender instanceof Player) {
				target = (Player)sender;
			} else {
				target = null;
			}
		} else {
			target = Bukkit.getPlayerExact(args[0]);
		}
		
		int duration = 30;
		
		if (args.length > 1) {
			try {
				duration = Integer.parseInt(args[1]);
			} catch (Exception e) {
				
			}
		}
				
		if (target == null || !target.isOnline()) {
			sender.sendMessage(ChatColor.RED + "Invalid target specified");
			return true;
		}
		
		// >=)
		if (duration > 0) {
			target.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, duration*20, 1), true);
			target.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, duration*20, 1), true);
			target.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, duration*20, 4), true);
			target.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, duration*20, 4), true);
			target.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, duration*20, 4), true);
			sender.sendMessage("Incapacitating " + target.getName());
		}
		return true;
		
	}

	@Override
	public void registerModule(AbeoAdmin instance) {
		instance.getCommand("incap").setExecutor(this);
	}

}
