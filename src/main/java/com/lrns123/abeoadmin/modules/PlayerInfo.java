package com.lrns123.abeoadmin.modules;

import java.util.Iterator;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;

import com.lrns123.abeoadmin.AbeoAdmin;

public class PlayerInfo implements IModule, CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(sender.hasPermission("abeoadmin.playerinfo")) {
			Player target;
			if(args.length > 0) {
				target = Bukkit.getPlayerExact(args[0]);
			} else if(sender instanceof Player) {
				target = (Player) sender;
			} else {
				target = null;
			}
			if(target == null) {
				sender.sendMessage(ChatColor.RED + "No target specified");
			}

			sender.sendMessage("Health: " + renderBar(ChatColor.GRAY + "[", target.getHealth(), target.getMaxHealth()));
			sender.sendMessage("Hunger: " + renderBar(ChatColor.GRAY + "[", target.getFoodLevel(), + 20));
			sender.sendMessage("Level: " + ChatColor.GREEN + target.getLevel());
			for(ItemStack item : target.getInventory().getArmorContents()) {
				sender.sendMessage("Armor: " + ChatColor.GOLD + item.getType().toString() + " " + ChatColor.RED + item.getDurability() + "/" + item.getType().getMaxDurability());
				StringBuilder sb = new StringBuilder();
				for(Enchantment ench : item.getEnchantments().keySet()) {
					sb.append(ench.getName());
					sb.append(" ");
					sb.append(item.getEnchantmentLevel(ench));
					sb.append(", ");
				}
				String enchants = sb.toString();
				if(!enchants.isEmpty())
					sender.sendMessage(" Enchants: " + ChatColor.AQUA + enchants);
			}
			
			if(target.getItemInHand().getType() != Material.AIR) {
				sender.sendMessage("HeldItem: " + target.getItemInHand().getType().toString());
				StringBuilder sb = new StringBuilder();
				for(Enchantment ench : target.getItemInHand().getEnchantments().keySet()) {
					sb.append(ench.getName());
					sb.append(" ");
					sb.append(target.getItemInHand().getEnchantmentLevel(ench));
					sb.append(", ");
				}
				String enchants = sb.toString();
				if(!enchants.isEmpty())
					sender.sendMessage(" Enchants: " + ChatColor.AQUA + enchants);
				
			}
			
			if(target.getActivePotionEffects().isEmpty())
				return true;

			Iterator<PotionEffect> potionEffects = target.getActivePotionEffects().iterator();
			sender.sendMessage("Effects:");
			while (potionEffects.hasNext()) {
				PotionEffect pe = potionEffects.next();
				sender.sendMessage(ChatColor.BLUE + pe.getType().getName() +":"+ ChatColor.AQUA + " Strength: " + (pe.getAmplifier() + 1) + " Duration: " + pe.getDuration());
			}
			
			
		}
		return false;
	}

	@Override
	public void registerModule(AbeoAdmin instance) {
		instance.getCommand("playerinfo").setExecutor(this);
		
	}

	private String renderBar(String bar, double d, double e) {
		for(int i = 0; i < e; i++) {
			if(i <= d) {
				bar += ChatColor.GREEN + "||";
			} else {
				bar += ChatColor.RED + "||";
			}
		}
		return (bar + ChatColor.GRAY + "]");
	}

}
