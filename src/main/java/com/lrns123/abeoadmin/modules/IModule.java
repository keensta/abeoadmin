package com.lrns123.abeoadmin.modules;

import com.lrns123.abeoadmin.AbeoAdmin;

public interface IModule {
	public void registerModule(AbeoAdmin instance);
}
