package com.lrns123.abeoadmin.modules;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;

import com.lrns123.abeoadmin.AbeoAdmin;

public class LockdownCommand implements IModule, CommandExecutor, Listener {

	boolean lockdown = false;
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if (!sender.hasPermission("abeoadmin.lockdown")) {
			sender.sendMessage(ChatColor.RED + "You do not have permission to use this command!");
			return true;
		}
		
		if(lockdown) {
			lockdown = false;
			Bukkit.broadcastMessage("Server lockdown lifted!");
			return true;
		}
		
		for(Player p : Bukkit.getOnlinePlayers()) {
			if(p.hasPermission("abeoadmin.lockdown"))
				continue;
			else {
				p.kickPlayer("We have just put the server into lockdown. This is to update or fix a issue with the server.");
			}
		}
		
		lockdown = true;
		Bukkit.broadcastMessage("Server is now in lockdown. People with 'abeoadmin.lockdown' permission only!");
		
		return false;
	}

	@Override
	public void registerModule(AbeoAdmin instance) {
		instance.getCommand("lockdown").setExecutor(this);
		instance.getServer().getPluginManager().registerEvents(this, instance);
	}
	
	@EventHandler
	public void playerLogin(PlayerLoginEvent ev) {
		
		if(lockdown) {
			if(ev.getPlayer().hasPermission("abeoadmin.lockdown")) {
				ev.allow();
				return;
			} else {
				ev.disallow(Result.KICK_OTHER, "Server is currently down for maintenance, Staff access only!");
			}
		}
		
	}

}
