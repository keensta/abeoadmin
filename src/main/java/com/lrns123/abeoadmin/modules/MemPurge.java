package com.lrns123.abeoadmin.modules;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import com.lrns123.abeoadmin.AbeoAdmin;

public class MemPurge implements CommandExecutor, IModule {

	@Override
	public void registerModule(AbeoAdmin instance) {
		instance.getCommand("mempurge").setExecutor(this);

	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (sender.hasPermission("abeoadmin.mempurge")) {
			sender.sendMessage(ChatColor.BLUE + "Purging memory...");
			System.gc();
			return true;
		}
		return false;
	}
}
