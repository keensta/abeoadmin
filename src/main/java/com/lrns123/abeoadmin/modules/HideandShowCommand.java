package com.lrns123.abeoadmin.modules;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.lrns123.abeoadmin.AbeoAdmin;

/**
 * 
 * @author Andi keen
 * 
 */

public class HideandShowCommand implements IModule, CommandExecutor, Listener  {
	
	
	List<String> hiddenPlayers = new ArrayList<String>();

	@Override
	public void registerModule(AbeoAdmin instance) {
		instance.getCommand("hide").setExecutor(this);
		instance.getCommand("show").setExecutor(this);
		instance.getServer().getPluginManager().registerEvents(this, instance);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,String[] args) {	
	
		if(!(sender instanceof Player))
			return true;
		
		Player player = (Player) sender;
		if(cmd.getName().equalsIgnoreCase("hide")) {
			hiddenPlayers.add(player.getName());
			for(Player target : Bukkit.getOnlinePlayers()) {
				if(target.hasPermission("abeoadmin.admin"))
					continue;
				target.hidePlayer(player);
			}
			player.sendMessage("You are now hidden from all players!");
			return true;
		} 
		if(cmd.getName().equalsIgnoreCase("show")){
			hiddenPlayers.remove(player.getName());
			for(Player target : Bukkit.getOnlinePlayers()) {
				target.showPlayer(player);
			}
			player.sendMessage("You are now visible to all players!");
			return true;
		}
		return false;
	}
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent ev) {
		Player joinP = ev.getPlayer();
		for(int i=0; i<hiddenPlayers.size();i++) {
			Player hiddenP = Bukkit.getPlayerExact(hiddenPlayers.get(i));
					
			if(hiddenP == null) {
				hiddenPlayers.remove(i);
				continue;
			}
			joinP.hidePlayer(hiddenP);
		}
	}
	
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent ev) {
		Player quitP = ev.getPlayer();
		if(hiddenPlayers.contains(quitP.getName())) {
			hiddenPlayers.remove(quitP.getName());
			for(Player onlineP : Bukkit.getOnlinePlayers()) {
				onlineP.showPlayer(quitP);
			}
		}
		
		for(int i=0; i<hiddenPlayers.size();i++) {
			Player hiddenP = Bukkit.getPlayerExact(hiddenPlayers.get(i));
					
			if(hiddenP == null) {
				hiddenPlayers.remove(i);
				continue;
			}
			
			quitP.hidePlayer(hiddenP);
		}
	}


}



