package com.lrns123.abeoadmin.modules;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.MemoryConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.lrns123.abeoadmin.AbeoAdmin;

public class KitCommand implements CommandExecutor, IModule {

	private class Enchant {
		public int id;
		public int lvl;
	}

	private class Item {
		public int itemId;
		public int subId;
		public int quantity;
		public List<Enchant> enchants = new ArrayList<Enchant>();
	}

	private class Kit {
		public List<Item> items = new ArrayList<Item>();
	}

	private Map<String, Kit> kits = new HashMap<String, Kit>();

	private AbeoAdmin plugin;
	private Logger log = Logger.getLogger("Minecraft");

	@Override
	public void registerModule(AbeoAdmin instance) {
		plugin = instance;
		instance.getCommand("kit").setExecutor(this);
		loadKits();
	}

	private void generateDefaultConfig() {
		File dataDirectory = plugin.getDataFolder();
		if (dataDirectory.exists() == false)
			dataDirectory.mkdirs();

		File f = new File(plugin.getDataFolder(), "kits.yml");
		if (f.canRead())
			return;

		log.info("[AbeoAdmin] kits.yml not found, generating default kits.yml...");
		InputStream in = getClass().getResourceAsStream("/kits.yml");

		if (in == null) {
			log.severe("[AbeoAdmin] Could not find kits.yml resource");
			return;
		} else {
			FileOutputStream fos = null;
			try {
				fos = new FileOutputStream(f);
				byte[] buf = new byte[512];
				int len;
				while ((len = in.read(buf)) > 0) {
					fos.write(buf, 0, len);
				}
			} catch (IOException iox) {
				log.severe("[AbeoAdmin] Could not export kits.yml");
				return;
			} finally {
				if (fos != null)
					try {
						fos.close();
					} catch (IOException iox) {
					}
				if (in != null)
					try {
						in.close();
					} catch (IOException iox) {
					}
			}
			return;
		}
	}

	@SuppressWarnings("unchecked")
	private ConfigurationSection mapToConfiguration(Map<?, ?> map) {		
		MemoryConfiguration memCfg = new MemoryConfiguration();
		memCfg.addDefaults((Map<String, Object>) map);

		return memCfg.getDefaultSection();
	}

	private void loadKits() {
		kits.clear();

		File f = new File(plugin.getDataFolder(), "kits.yml");
		if (!f.canRead())
			generateDefaultConfig();

		if (!f.canRead()) {
			log.severe("[AbeoAdmin] Cannot read kits.yml!");
			return;
		}

		YamlConfiguration config = YamlConfiguration.loadConfiguration(f);

		if (!config.contains("kits")) {
			log.severe("[AbeoAdmin] No kits defined!");
			return;
		}

		Set<String> kits = config.getConfigurationSection("kits").getKeys(false);
		for (String kitId : kits) {
			Kit kit = new Kit();
			List<Map<?, ?>> items = config.getMapList("kits." + kitId);
			for (Map<?, ?> itm : items) {
				ConfigurationSection itemData = mapToConfiguration(itm);

				Item item = new Item();
				item.itemId = itemData.getInt("itemId", 0);
				item.subId = itemData.getInt("subId", 0);
				item.quantity = itemData.getInt("quantity", 1);

				if (itemData.contains("enchant")) {
					List<Map<?, ?>> enchants = itemData.getMapList("enchant");
					for (Map<?, ?> enchant : enchants) {
						ConfigurationSection enchData = mapToConfiguration(enchant);

						Enchant ench = new Enchant();
						ench.id = enchData.getInt("id", -1);
						ench.lvl = enchData.getInt("lvl", 0);

						item.enchants.add(ench);
					}
				}
				kit.items.add(item);
			}

			this.kits.put(kitId.toLowerCase(), kit);
		}
	}

	@SuppressWarnings("deprecation")
	private void giveKit(Player ply, Kit kit) {
		for (Item item : kit.items) {
			ItemStack itm = new ItemStack(item.itemId, item.quantity, (short) item.subId);
			for (Enchant ench : item.enchants) {
				itm.addUnsafeEnchantment(Enchantment.getById(ench.id), ench.lvl);
			}
			ply.getInventory().addItem(itm);
		}
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (sender instanceof Player && sender.hasPermission("abeoadmin.kit")) {
			if (args.length < 1) {
				sender.sendMessage(ChatColor.RED + "Please specify the kit to spawn");
			} else {
				if (args[0].equalsIgnoreCase("reload")) {
					loadKits();
					sender.sendMessage(ChatColor.BLUE + "Kits reloaded");
				} else {
					String kitId = args[0].toLowerCase();
					if (kits.containsKey(kitId)) {
						Kit kit = kits.get(kitId);
						giveKit((Player) sender, kit);
						sender.sendMessage(ChatColor.BLUE + "Kit '" + kitId + "' spawned!");
					} else {
						sender.sendMessage(ChatColor.RED + "Unknown kit ID");
					}
				}
			}
		}
		return true;
	}

}
