package com.lrns123.abeoadmin.modules;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;

import com.lrns123.abeoadmin.AbeoAdmin;

/**
 * 
 * @author Andi keen
 * 
 */

public class ClearCommand implements IModule, CommandExecutor  {
	

	@Override
	public void registerModule(AbeoAdmin instance) {
		instance.getCommand("clear").setExecutor(this);
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,String[] args) {
		Player player = (Player) sender;
		if(cmd.getName().equalsIgnoreCase("clear")){
			if(player.hasPermission("abeoadmin.clear")){
				
				if(args.length == 0){
					player.sendMessage(ChatColor.RED + "Usage: To clear your own /clear inv");
					player.sendMessage(ChatColor.RED + "Usage: To clear targets /clear inv [Playername]");
					player.sendMessage(ChatColor.RED + "Usage: To clear chest /clear chest -Make sure to aim at the chest");
				}else if(args.length > 0){
					if(args[0].equalsIgnoreCase("inv")){
						if(args.length == 1){
						player.getInventory().clear();
						player.sendMessage(ChatColor.RED + "Inventory is now clear.");
						}else if(args.length == 2){
							Player target = Bukkit.getPlayerExact(args[1]);
							target.getInventory().clear();
							player.sendMessage(ChatColor.RED + target.getName() + " inventory got cleared.");
							target.sendMessage(ChatColor.GOLD + "Your Inventory just got cleared by a Mod/Admin");
						}
					}else if(args[0].equalsIgnoreCase("chest")){
						Block target = player.getTargetBlock(null, 100);
						if (target.getState() instanceof InventoryHolder) {
							Inventory inventory = ((InventoryHolder) target.getState()).getInventory();
							inventory.clear();
							player.sendMessage(ChatColor.GOLD + "Chest has been cleared");
						}else{
						player.sendMessage(ChatColor.RED + "You need to aim at a chest for this to work");
						}
					}
				}
			}else{
				player.sendMessage(ChatColor.RED + "You don't have permission to view this.");
			}
		}
		return false;
	}

}


