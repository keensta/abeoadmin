package com.lrns123.abeoadmin.modules;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.lrns123.abeoadmin.AbeoAdmin;

public class SavePlayersCommand implements IModule, CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] arg3) {
		for (Player ply : Bukkit.getOnlinePlayers()) {
			ply.saveData();
		}
		sender.sendMessage(ChatColor.BLUE + "Player data saved");
		return false;
	}

	@Override
	public void registerModule(AbeoAdmin instance) {
		instance.getCommand("save-players").setExecutor(this);

	}

}
