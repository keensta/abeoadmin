package com.lrns123.abeoadmin.modules;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Minecart;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.vehicle.VehicleEnterEvent;
import org.bukkit.event.vehicle.VehicleExitEvent;
import org.bukkit.event.vehicle.VehicleMoveEvent;

import com.lrns123.abeoadmin.AbeoAdmin;

public class MinecartSpeed implements CommandExecutor, IModule, Listener {

    AbeoAdmin instance;
    private HashMap<String, Double> playersSpeed = new HashMap<String, Double>();
    private List<String> inMC = new ArrayList<String>();
    
    @Override
    public void registerModule(AbeoAdmin instance) {
        this.instance = instance;

        instance.getCommand("cart").setExecutor(this);
        instance.getServer().getPluginManager().registerEvents(this, instance);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if(!(sender instanceof Player))
            return true;

        if(sender.hasPermission("abeoadmin.cart")) {
            Player p = (Player) sender;

                if(args.length == 0) {
                    p.sendMessage(ChatColor.RED + "Usage: /cart [speed]");
                    return true;
                }

                try {
                    Integer.parseInt(args[0]);
                } catch(NumberFormatException e) {
                    p.sendMessage(ChatColor.RED + "args[0] is not a number");
                    return true;
                }

                playersSpeed.put(p.getName(), Double.parseDouble(args[0]));
                p.sendMessage(ChatColor.GREEN + "Minecart speed multiplier set to " + args[0]);

        }

        return false;
    }

    @EventHandler
    public void onMinecartEnter(VehicleEnterEvent ev) {

        if(ev.getVehicle().getType() == EntityType.MINECART) {

            if(!(ev.getEntered() instanceof Player))
                return;

            Player p = (Player) ev.getEntered();

            if(!p.hasPermission("abeoadmin.cart"))
                return;

            inMC.add(p.getName());
        }

    }

    @EventHandler
    public void onMinecartExit(VehicleExitEvent ev) {

        if(ev.getVehicle().getType() == EntityType.MINECART) {

            if(!(ev.getExited() instanceof Player))
                return;

            Player p = (Player) ev.getExited();

            if(inMC.contains(p.getName()))
                inMC.remove(p.getName());

        }

    }

    @EventHandler
    public void onMinecartMove(VehicleMoveEvent ev) {

        if(ev.getVehicle().getType() == EntityType.MINECART) {
            if(ev.getVehicle().getPassenger() instanceof Player) {
                Player p = (Player) ev.getVehicle().getPassenger();

                if(playersSpeed.containsKey(p.getName())) {
                    Minecart mc = (Minecart) ev.getVehicle();
                    mc.setMaxSpeed(0.4 * playersSpeed.get(p.getName()));
                }
            }
        }

    }

}
