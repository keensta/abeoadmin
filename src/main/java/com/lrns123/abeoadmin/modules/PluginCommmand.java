package com.lrns123.abeoadmin.modules;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;
import com.lrns123.abeoadmin.AbeoAdmin;

public class PluginCommmand implements IModule, CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(!sender.hasPermission("abeoadmin.plugins"))
			return true;
		
		sender.sendMessage("Plugins " + getPluginList());
		return true;
	  }
	   
	   private String getPluginList() {
		   StringBuilder pluginList = new StringBuilder();
		   Plugin[] plugins = Bukkit.getPluginManager().getPlugins();
	   
		   for (Plugin plugin : plugins) {
			   if (pluginList.length() > 0) {
				   pluginList.append(ChatColor.WHITE);
				   pluginList.append(", ");
			   }
	   
			   pluginList.append(plugin.isEnabled() ? ChatColor.GREEN : ChatColor.RED);
			   pluginList.append(plugin.getDescription().getName());
		   }
	    
		   return "(" + plugins.length + "): " + pluginList.toString();
	   }

	@Override
	public void registerModule(AbeoAdmin instance) {
		instance.getCommand("plug").setExecutor(this);
	}

}
