package com.lrns123.abeoadmin.modules;

import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.lrns123.abeoadmin.AbeoAdmin;

public class LightningCommand implements CommandExecutor, IModule {

	@Override
	public void registerModule(AbeoAdmin instance) {
		// TODO Auto-generated method stub
		instance.getCommand("lightning").setExecutor(this);
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		// TODO Auto-generated method stub
		
		if (!(sender instanceof Player)) {
			   return true;
		}
		
		if (!sender.hasPermission("abeoadmin.lightning"))
			   return true;
		
		Player player = (Player)sender;
		Block target = player.getTargetBlock(null, 600);
		
		if (target != null)
			player.getWorld().strikeLightning(target.getLocation());
		return true;
	}

}
