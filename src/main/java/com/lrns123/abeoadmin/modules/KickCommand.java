package com.lrns123.abeoadmin.modules;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.lrns123.abeoadmin.AbeoAdmin;

public class KickCommand implements IModule, CommandExecutor{
    
public AbeoAdmin plugin;
    
    @Override
    public void registerModule(AbeoAdmin instance) {
        this.plugin = instance;
        
        plugin.getCommand("kick").setExecutor(this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(!(sender instanceof Player))
            return true;
        
        final Player ply = (Player) sender;
        if(!(ply.hasPermission("abeoadmin.ban")))
            return true;
        
        if(args.length == 0) {
            sender.sendMessage(ChatColor.RED + "/kick [Player] [Reason]");
        }
        
        if(args[0].equalsIgnoreCase("keensta")) {
            ply.kickPlayer("Keensta can't be kicked.");
            return true;
        }
        
        if(args.length == 1) {
           Player target = Bukkit.getPlayer(args[0]);
           
           if(!target.isOnline() || target == null) {
               sender.sendMessage(ChatColor.RED + "Either they are offline or the name is incorrect");
               return true;
           }
           
           target.kickPlayer("You've been kicked!");
           Bukkit.broadcast(ChatColor.GRAY + "["+ply.getName()+"] Kicked " + target.getName() + " Reason: N/A", "abeoadmin.ban");
        }
        
        if(args.length == 2) {
            Player target = Bukkit.getPlayer(args[0]);
            
            if(!target.isOnline() || target == null) {
                sender.sendMessage(ChatColor.RED + "Either they are offline or the name is incorrect");
                return true;
            }
            
            StringBuilder message = new StringBuilder();
            for(int i = 1; i < args.length; i++) {
                if(i > 1)
                    message.append(" ");
                message.append(args[i]);
            }
            
            target.kickPlayer(message.toString());
            Bukkit.broadcast(ChatColor.GRAY + "["+ply.getName()+"] Kicked " + target.getName() + " Reason: " + message.toString(), "abeoadmin.ban");
        }
        
        return false;
    }

}
