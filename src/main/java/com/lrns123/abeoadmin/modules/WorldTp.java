package com.lrns123.abeoadmin.modules;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import com.lrns123.abeoadmin.AbeoAdmin;

public class WorldTp implements IModule, CommandExecutor  {
	

	@Override
	public void registerModule(AbeoAdmin instance) {
		instance.getCommand("worldtp").setExecutor(this);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,String[] args) {
		Player player = (Player) sender;
		if(cmd.getName().equalsIgnoreCase("worldtp")){
			if(!player.hasPermission("abeoadmin.worldtp")) {
				player.sendMessage(ChatColor.RED + "You do not have permission to use this command");
				return true;
			}
			if(args.length == 0) {
				player.sendMessage(ChatColor.RED + "Not enought arguments. Use /worldtp [Worldname]");
				return true;
			}
			
			World world = Bukkit.getWorld(args[0]);
			
			if(world == null) {
				player.sendMessage(ChatColor.RED + "World does not exist. Please check the name you entered!");
				return true;
			}
			
			Location loc = world.getSpawnLocation();
			
			player.teleport(new Location(world, loc.getX(), loc.getY(), loc.getZ()), TeleportCause.COMMAND);
		}
		return false;
	}

}


