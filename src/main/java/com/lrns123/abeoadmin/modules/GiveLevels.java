package com.lrns123.abeoadmin.modules;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.lrns123.abeoadmin.AbeoAdmin;

public class GiveLevels implements IModule, CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!sender.hasPermission("abeoadmin.givelevels"))
			return true;

		if (args.length < 2) {
			sender.sendMessage(ChatColor.RED + "Syntax: /givelevels <name> <levels>");
		} else {
			Player ply = Bukkit.getPlayerExact(args[0]);
			if (ply == null) {
				sender.sendMessage(ChatColor.RED + "Player not found");
			} else {
				int levels = -1;
				try {
					levels = Integer.parseInt(args[1]);

				} catch (NumberFormatException e) {
					sender.sendMessage(ChatColor.RED + "Invalid levels specified");
				}

				if (levels > 0)
					giveExpLevels(ply, levels);
			}
		}
		return true;
	}

	private void giveExpLevels(Player ply, int levels) {
		int targetLevel = ply.getLevel() + levels;

		ply.setExp(0);
		ply.setLevel(0);
		ply.setTotalExperience(0);
		while (targetLevel > 0) {
			final int expToLevel = (7 + (ply.getLevel() * 7 >> 1));
			ply.giveExp(expToLevel);
			targetLevel--;
		}

	}

	@Override
	public void registerModule(AbeoAdmin instance) {
		instance.getCommand("givelevels").setExecutor(this);
	}

}
