package com.lrns123.abeoadmin.modules;


import java.io.File;
import java.io.IOException;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.Listener;

import com.lrns123.abeoadmin.AbeoAdmin;

public class PardonCommand implements IModule, CommandExecutor, Listener{
	
	public AbeoAdmin plugin;
	
	File banFile = new File("plugins/AbeoAdmin", "bans.yml");
	YamlConfiguration bans = YamlConfiguration.loadConfiguration(banFile);
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(cmd.getLabel().equalsIgnoreCase("pardon")) {
			if(sender.hasPermission("abeoadmin.pardon")) {
				if(args.length == 1) {
					 sender.sendMessage(ChatColor.GRAY + "You have pardoned: " + ChatColor.RED + args[0]);
					 bans.set(args[0], null);
					 try {
						bans.save(banFile);
					} catch (IOException e) {
						sender.sendMessage(ChatColor.RED + "There was an error saving the bans.yml file");
					}
				} else {
					sender.sendMessage(ChatColor.RED + "Usage: /pardon [player]");
				}
			} else {
				sender.sendMessage(ChatColor.RED + "Insufficient permissions");
			}
			return true;
		}
		return false;
	}


	@Override
	public void registerModule(AbeoAdmin instance) {
		this.plugin = instance;
		plugin.getCommand("pardon").setExecutor(this);
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
}
