package com.lrns123.abeoadmin.modules;

import java.util.HashMap;
import java.util.HashSet;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.PluginManager;
import com.lrns123.abeoadmin.AbeoAdmin;
import com.lrns123.abeoadmin.runnables.SilenceExpireTask;

public class PlayerSilence implements Listener, CommandExecutor, IModule {
	// JavaPlugin
	private AbeoAdmin plugin = null;

	private HashMap<String, Integer> silencedPlayers = new HashMap<String, Integer>();
	private HashSet<String> pendingUnsilence = new HashSet<String>();

	public void registerModule(AbeoAdmin instance) {
		plugin = instance;

		PluginManager pm = plugin.getServer().getPluginManager();
		pm.registerEvents(this, plugin);

		plugin.getCommand("silence").setExecutor(this);
		plugin.getCommand("silencetime").setExecutor(this);
		plugin.getCommand("unsilence").setExecutor(this);
	}

	public void silencePlayer(Player ply, int time) {
		String plyN = ply.getName();
		silencedPlayers.put(plyN, time);
	}

	public void unsilencePlayer(Player ply) {
		String plyN = ply.getName();
		silencedPlayers.remove(plyN);
	}

	public boolean isSilenced(Player ply) {
		String plyN = ply.getName();
		if (silencedPlayers.containsKey(plyN))
			return true;
		return false;
	}

	public void unsilenceAll() {
		silencedPlayers.clear();
	}

	public void silenceExpire(String ply) {
		Player plyP = Bukkit.getPlayerExact(ply);
		if (plyP != null && plyP.isOnline() && isSilenced(plyP)) {
			plyP.sendMessage(ChatColor.RED + "You are now unsilenced");
			Command.broadcastCommandMessage(plyP, "Unsilencing player " + plyP.getName());
			unsilencePlayer(plyP);
		} else {
			if(isSilenced(plyP)) {
				pendingUnsilence.add(ply);
			} else {
				return;
			}
			
		}
	}

	@EventHandler
	public void onPlayerChat(AsyncPlayerChatEvent ev) {
		Player ply = ev.getPlayer();
		if (isSilenced(ply))
			ev.setCancelled(true);
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent ev) {
		Player ply = ev.getPlayer();
		String plyN = ply.getName();
		if (pendingUnsilence.contains(plyN)) {
			pendingUnsilence.remove(plyN);
			silenceExpire(plyN);
		}
	}
	
	@EventHandler
	public void onPlayerCommand(PlayerCommandPreprocessEvent ev) {
		String cmd = ev.getMessage().toLowerCase();
		if(cmd.startsWith("/me") || cmd.startsWith("/r") || cmd.startsWith("/msg") || cmd.startsWith("/tell")) {
			if(isSilenced(ev.getPlayer())) {
				ev.setCancelled(true);
			}
		}
	}

	@EventHandler
	public void onPlayerLeave(PlayerQuitEvent ev) {
		Player ply = ev.getPlayer();
		String plyN = ply.getName();
		if(silencedPlayers.containsKey(plyN)){
		int time = silencedPlayers.get(plyN);
		if (time == 0)
			silencedPlayers.remove(plyN);
		}
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!sender.hasPermission("abeoadmin.silence"))
			return true;

		if (cmd.getName().equalsIgnoreCase("silence")) {
			if (args.length < 1) {
				sender.sendMessage(ChatColor.RED + "Syntax: /silence [Name] <Length>");
			} else {
				Player ply = Bukkit.getPlayerExact(args[0]);
				if (ply == null) {
					sender.sendMessage(ChatColor.RED + "Player not found");
				} else {
					if (isSilenced(ply)) {
						sender.sendMessage("This player is already silenced");
					} else {
						if (args.length == 1) {
							int time = 0;
							Command.broadcastCommandMessage(sender, "Silencing player " + ply.getName());
							ply.sendMessage(ChatColor.RED + "You have been silenced.");
							silencePlayer(ply, time);
						} else {
							try {
								int time = Integer.parseInt(args[1]);
								Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new SilenceExpireTask(this, ply.getName()), ((time * 10) * 6) * 20);
								Command.broadcastCommandMessage(sender, "Silencing player " + ply.getName() + " For " + time + " minute(s).");
								ply.sendMessage(ChatColor.RED + "You have been silenced." + " For " + time + " minute(s).");
								silencePlayer(ply, time);
							} catch (Exception e) {
								// TODO: handle exception
								sender.sendMessage(ChatColor.RED + "Something failed contact a developer");
								sender.sendMessage(ChatColor.RED + "Try /silence [name] <time>  (Time has to be a number)");
							}

						}
					}
				}
			}
		} else if (cmd.getName().equalsIgnoreCase("unsilence")) {
			if (args.length < 1) {
				sender.sendMessage(ChatColor.RED + "Syntax: /unmute <name>");
			} else {
				if (args[0] == "*") {
					unsilenceAll();
					Command.broadcastCommandMessage(sender, "Unsilencing all players");
				} else {

					Player ply = Bukkit.getPlayerExact(args[0]);
					if (ply == null) {
						sender.sendMessage(ChatColor.RED + "Player not found");
					} else {
						if (!isSilenced(ply)) {
							sender.sendMessage("This player is not muted");
						} else {
							unsilencePlayer(ply);
							Command.broadcastCommandMessage(sender, "Unsilencing player " + ply.getName());
							ply.sendMessage(ChatColor.RED + "You have been unsilenced.");
						}
					}
				}
			}
		} else if (cmd.getName().equalsIgnoreCase("silencetime")) {
			if (args.length == 1) {
				Player ply = Bukkit.getPlayerExact(args[0]);
				if (ply == null) {
					sender.sendMessage(ChatColor.RED + "Player not found");
				} else {
					if (silencedPlayers.containsKey(ply.getName())) {
						int timeLeft = silencedPlayers.get(ply.getName());
						sender.sendMessage(ChatColor.RED + "Remaining silence time for " + ply.getName() + " is " + timeLeft);
					} else {
						sender.sendMessage(ChatColor.RED + "This player is not muted");
					}
				}
			}
		}
		return true;
	}
}
