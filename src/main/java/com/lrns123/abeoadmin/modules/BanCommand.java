package com.lrns123.abeoadmin.modules;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandException;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;

import com.lrns123.abeoadmin.AbeoAdmin;

public class BanCommand implements IModule, CommandExecutor, Listener {

    public AbeoAdmin plugin;

    HashMap<String, String> autobanData = new HashMap<String, String>();
    long minute = 60000;
    long hour = minute * 60;
    long day = hour * 24;
    long week = day * 7;

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if(cmd.getLabel().equalsIgnoreCase("ban")) {
            long ct = System.currentTimeMillis();
            if(sender.hasPermission("abeoadmin.ban")) {
                if((args.length == 0) || (args.length >= 3)) {
                    sender.sendMessage(ChatColor.RED + "Usage: /ban [player] {time} {M/H/D/W}");
                }
                if(args.length == 1) {
                    String tName = args[0];
                    banPlayer(tName, sender);
                }
                if(args.length == 2) {
                    long bl = getBanLength(args[1]);
                    long ut = ct + bl;

                    tbanPlayer(args[0], ut, bl, sender);
                }
            } else {
                sender.sendMessage(ChatColor.RED + "Insufficient permissions");
            }
            return true;
        } else if(cmd.getLabel().equalsIgnoreCase("autoban")) {
            if(sender.hasPermission("abeoadmin.ban")) {
                if(args.length == 0) {
                    sender.sendMessage(ChatColor.RED + "Usage: /autoban [player] {time} {M/H/D/W}");
                    sender.sendMessage(ChatColor.RED
                            + "Usage: /autoban {reason/appeal/evidence/additional} [detail]");
                    return true;
                }
                
                if(args.length == 1) {
                    if(args[0].equalsIgnoreCase("complete")) {
                        sender.sendMessage(ChatColor.BLUE + "DEBUG- DATA- " + autobanData.get(sender.getName()));
                        try {
                            final URL url = new URL("http://api.abeomc.net/ban/?key=8E773168x5oz449oG4ns6P19Avez9R&"
                                    + autobanData.get(sender.getName()) + "}");
                            final URLConnection con = url.openConnection();

                            // Get the response
                            final BufferedReader rd = new BufferedReader(new InputStreamReader(con.getInputStream()));

                            String response = "";
                            String line;
                            while((line = rd.readLine()) != null) {
                                response = response + line;
                            }

                            rd.close();
                            plugin.getLogger().info("[AbeoAdmin] Forum notified of new rank, response: " + response);
                        } catch(final Exception e) {
                            plugin.getLogger().severe("[AbeoAdmin] Error sending forum notification");
                            e.printStackTrace();
                        }
                    }
                }
                    String[] subCommands = { "reason", "appeal", "evidence", "additional", "Reason", "Appeal",
                            "Evidence", "Additional" };

                    if(args.length < 2 && Arrays.asList(subCommands).contains(args[0])) {
                        sender.sendMessage(ChatColor.RED + "Usage: /autoban [player] {time} {M/H/D/W}");
                        sender.sendMessage(ChatColor.RED
                                + "Usage: /autoban {reason/appeal/evidence/additional} [detail]");
                        return true;
                    }

                    if(args.length >= 2 && Arrays.asList(subCommands).contains(args[0])) {

                        if(!autobanData.containsKey(sender.getName())) {
                            sender.sendMessage(ChatColor.RED + "Do '/autoban [player] {time} {M/H/D/W}' first");
                            return true;
                        }

                        if(args[0].equalsIgnoreCase("reason")) {
                            StringBuilder reason = new StringBuilder();
                            for(int i = 1; i < args.length; i++) {
                                if(i > 1)
                                    reason.append(" ");
                                reason.append(args[i]);
                            }
                            String data = autobanData.get(sender.getName());
                            data = data + ",\"reason\":\"" + reason.toString() + "\"";
                            autobanData.put(sender.getName(), data);
                            
                            sender.sendMessage(ChatColor.BLUE + "Reason: " + reason.toString());
                        }

                        if(args[0].equalsIgnoreCase("appeal")) {
                            Boolean b = Boolean.getBoolean(args[0]);
                            String data = autobanData.get(sender.getName());

                            data = data + ",\"appeal\"" + b + "";
                            autobanData.put(sender.getName(), data);
                            
                            sender.sendMessage(ChatColor.BLUE + "Appeal: " + b);
                        }

                        if(args[0].equalsIgnoreCase("evidence")) {
                            StringBuilder evidence = new StringBuilder();
                            for(int i = 1; i < args.length; i++) {
                                if(i > 1)
                                    evidence.append(" ");
                                evidence.append(args[i]);
                            }
                            String data = autobanData.get(sender.getName());
                            data = data + ",\"evidence\":\"" + evidence.toString() + "\"";
                            autobanData.put(sender.getName(), data);
                            
                            sender.sendMessage(ChatColor.BLUE + "Evidence: " + evidence.toString());
                        }

                        if(args[0].equalsIgnoreCase("additional")) {
                            StringBuilder additional = new StringBuilder();
                            for(int i = 1; i < args.length; i++) {
                                if(i > 1)
                                    additional.append(" ");
                                additional.append(args[i]);
                            }
                            String data = autobanData.get(sender.getName());
                            data = data + ",\"additional\":\"" + additional.toString() + "\"";
                            autobanData.put(sender.getName(), data);
                            
                            sender.sendMessage(ChatColor.BLUE + "Additional: " + additional.toString());
                        }
                        return true;
                    }

                    /*
                     * "player": "test", "staff": "shootingsignals",
                     * "timestamp": 12345678910, "duration": "34 days",
                     * "reason": "test", "appeal": true, "evidence": "None",
                     * "additional": "None"
                     */

                    String tName = args[0];
                    String staff = sender.getName();
                    int timestamp = (int) (System.currentTimeMillis() / 1000);
                    String duration = getTimeString(getBanLength(args[1]));

                    String data = "data={\"player\":\"" + tName + "\",\"staff\":\"" + staff + "\",\"timestamp\":"
                            + timestamp + ",\"duration\":\"" + duration + "\"";

                    autobanData.put(sender.getName(), data);
                    sender.sendMessage(ChatColor.GREEN
                            + "Complete the ban by doing '/autoban {reason/appeal/evidence/additional} [details or true/false]' (CaseSensitive)");
                    sender.sendMessage(ChatColor.GREEN + "Do it in to order shown above, then do '/autoban complete'");
                } else {
                    sender.sendMessage(ChatColor.RED + "Insufficient permissions");
                }
        }
        return false;
    }

    @Override
    public void registerModule(AbeoAdmin instance) {
        this.plugin = instance;
        plugin.getCommand("ban").setExecutor(this);
        //plugin.getCommand("autoban").setExecutor(this);
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void playerJoinEvent(PlayerLoginEvent ev) {
        File banFile = new File("plugins/AbeoAdmin", "bans.yml");
        YamlConfiguration bans = YamlConfiguration.loadConfiguration(banFile);
        Player player = ev.getPlayer();
        String pName = player.getName();
        long ut = bans.getLong("players." + ev.getPlayer().getName() + ".UnbanTime");
        long ct = System.currentTimeMillis();
        if(bans.getBoolean("players." + pName + ".Banned") == true) {
            if(bans.getLong("players." + pName + ".UnbanTime") == -1) {
                ev.disallow(Result.KICK_BANNED, ChatColor.RED + "You are still banned!\n" + ChatColor.YELLOW
                        + "Please appeal on the forums.");
                return;
            }
            if(bans.getLong("players." + pName + ".UnbanTime") > ct) {
                long bl = ut - ct;
                String time = getTimeString(bl);
                ev.disallow(Result.KICK_BANNED, ChatColor.RED + "You are still banned for another\n" + ChatColor.YELLOW
                        + time + ChatColor.YELLOW + ChatColor.RED + "\nPlease appeal on the forums.");
            } else {
                bans.set("players." + pName, null);
                try {
                    bans.save(banFile);
                } catch(IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public void banPlayer(String tName, CommandSender sender) {
        File banFile = new File("plugins/AbeoAdmin", "bans.yml");
        YamlConfiguration bans = YamlConfiguration.loadConfiguration(banFile);
        Player target = Bukkit.getPlayer(tName);
        if(target != null) {
            target.kickPlayer(ChatColor.RED + "You have been banned!\n Please appeal on the forums.");
            bans.set("players." + tName + ".Banned", true);
            bans.set("players." + tName + ".UnbanTime", -1);
            sender.sendMessage(ChatColor.GRAY + "You have banned: " + ChatColor.RED + tName);
            try {
                bans.save(banFile);
            } catch(IOException e) {
                sender.sendMessage(ChatColor.RED + "There was an error saving the bans.yml file");
            }
        } else {
            bans.set("players." + tName + ".Banned", true);
            bans.set("players." + tName + ".UnbanTime", -1);
            sender.sendMessage(ChatColor.GRAY + "You have banned: " + ChatColor.RED + tName);
            try {
                bans.save(banFile);
            } catch(IOException e) {
                sender.sendMessage(ChatColor.RED + "There was an error saving the bans.yml file");
            }
        }
    }

    public void tbanPlayer(String tName, long ut, long bl, CommandSender sender) {
        File banFile = new File("plugins/AbeoAdmin", "bans.yml");
        YamlConfiguration bans = YamlConfiguration.loadConfiguration(banFile);
        String time = getTimeString(bl);
        bans.set("players." + tName + ".Banned", true);
        bans.set("players." + tName + ".UnbanTime", ut);
        try {
            bans.save(banFile);
        } catch(IOException e) {
            e.printStackTrace();
        }
        Player target = Bukkit.getPlayerExact(tName);
        if(target != null) {
            target.kickPlayer(ChatColor.RED + "You have been banned for\n " + ChatColor.YELLOW + time + ChatColor.RED
                    + "\nPlease appeal on the forums.");
        }
        sender.sendMessage(ChatColor.GRAY + "You have banned: " + ChatColor.RED + tName + ChatColor.GRAY + " for "
                + ChatColor.RED + time);
    }

    public String getTimeString(long bl) {
        {
            if(bl < 0) {
                throw new IllegalArgumentException("Duration must be above 0");
            }

            long hours = TimeUnit.MILLISECONDS.toHours(bl);
            bl -= TimeUnit.HOURS.toMillis(hours);
            long minutes = TimeUnit.MILLISECONDS.toMinutes(bl);
            bl -= TimeUnit.MINUTES.toMillis(minutes);

            StringBuilder sb = new StringBuilder(64);
            sb.append(hours);
            sb.append(" Hours and ");
            sb.append(minutes);
            sb.append(" Minutes");

            return (sb.toString());
        }
    }

    public long getBanLength(String time) throws CommandException {
        if(time == null)
            return 0L;
        if(time.equalsIgnoreCase("now"))
            return System.currentTimeMillis();
        String[] groupings = time.split("-");
        if(groupings.length == 0)
            throw new CommandException("Invalid date specified");
        Calendar cal = new GregorianCalendar();
        cal.setTimeInMillis(0);
        for(String str : groupings) {
            int type;
            switch(str.toLowerCase().charAt(str.length() - 1)) {
                case 'm':
                    type = Calendar.MINUTE;
                    break;
                case 'h':
                    type = Calendar.HOUR;
                    break;
                case 'd':
                    type = Calendar.DATE;
                    break;
                case 'w':
                    type = Calendar.WEEK_OF_YEAR;
                    break;
                default:
                    throw new CommandException("Unknown date value specified");
            }
            cal.add(type, Integer.valueOf(str.substring(0, str.length() - 1)));
        }
        return cal.getTimeInMillis();
    }

}
