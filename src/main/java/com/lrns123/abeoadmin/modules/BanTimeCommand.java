package com.lrns123.abeoadmin.modules;


import java.io.File;
import java.util.concurrent.TimeUnit;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.Listener;

import com.lrns123.abeoadmin.AbeoAdmin;

public class BanTimeCommand implements IModule, CommandExecutor, Listener{
	
	public AbeoAdmin plugin;
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(cmd.getLabel().equalsIgnoreCase("bantime")) {
			if(sender.hasPermission("abeoadmin.bantime")) {
				if((args.length == 0) || (args.length >= 2)) {
					sender.sendMessage(ChatColor.RED + "Usage: /bantime [player]");
				}
				if(args.length == 1) {
					File banFile = new File("plugins/AbeoAdmin", "bans.yml");
					YamlConfiguration bans = YamlConfiguration.loadConfiguration(banFile);
					String targetName = args[0];
					long ct = System.currentTimeMillis();
					if(bans.contains(targetName + ".UnbanTime") && (bans.getLong(targetName + ".UnbanTime") > ct)) {
						long ut = bans.getLong(targetName + ".UnbanTime");
						long bl = ut-ct;
						String length = getTimeString(bl);
						sender.sendMessage(ChatColor.GRAY + args[0] + " will be unbanned in: " + ChatColor.RED + length);
					} else {
						sender.sendMessage(ChatColor.RED + args[0] + ChatColor.GRAY + " is not temporary banned");
					}
				}
			} else {
				sender.sendMessage("Insufficient permissions");
			}
			return true;
		}
		return false;
	}


	@Override
	public void registerModule(AbeoAdmin instance) {
		this.plugin = instance;
		plugin.getCommand("bantime").setExecutor(this);
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	
	public String getTimeString(long bl) {
		{
	        if(bl < 0)
	        {
	            throw new IllegalArgumentException("Duration must be above 0");
	        }
	        
	        long hours = TimeUnit.MILLISECONDS.toHours(bl);
	        bl -= TimeUnit.HOURS.toMillis(hours);
	        long minutes = TimeUnit.MILLISECONDS.toMinutes(bl);
	        bl -= TimeUnit.MINUTES.toMillis(minutes);

	        StringBuilder sb = new StringBuilder(64);
	        sb.append(hours);
	        sb.append(" Hours and ");
	        sb.append(minutes);
	        sb.append(" Minutes");

	        return(sb.toString());
	    }
    }
}
