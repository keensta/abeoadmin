package com.lrns123.abeoadmin;

import java.util.HashMap;
import java.util.logging.Logger;

import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;

import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import com.lrns123.abeoadmin.modules.*;

/**
 * 
 * @author Lourens Elzinga
 * 
 */
public class AbeoAdmin extends JavaPlugin {

	Logger log = Logger.getLogger("Minecraft");

	// Modules
	HashMap<String, IModule> loadedModules = new HashMap<String, IModule>();
	
	public Economy economy;
	public Permission permission;

	@Override
	public void onDisable() {

		log.info(this.getDescription().getFullName() + " disabled");
	}

	@Override
	public void onEnable() {
		
		if (!setupEconomy()) {
			log.severe("[AbeoAdmin] Unable to find economy plugin!");
		}
		
		if (!setupPermissions()) {
			log.severe("[AbeoAdmin] Unable to find permissions plugin!");
		}

		try {
			initializeModules();
		} catch (Exception e) {
			log.severe("[AbeoAdmin] Failed to initialize modules: " + e.getMessage());
			e.printStackTrace();

			getServer().getPluginManager().disablePlugin(this);
			return;
		}

		//Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(this, new WorldSave(), (20 * 60)*15, (20 * 60)*15);
		log.info(this.getDescription().getFullName() + " enabled");
	}

	private void initializeModules() {
		loadedModules.put("silence", new PlayerSilence());
		//loadedModules.put("animalKill", new AnimalKillLogger());
		loadedModules.put("enchant", new EnchantCommand());
		loadedModules.put("resetend", new ResetEndCommand());
		loadedModules.put("mempurge", new MemPurge());
		loadedModules.put("signedit", new SignEdit());
		loadedModules.put("endernerf", new EnderNerf());
		loadedModules.put("portalcontrol", new PortalControl());
		loadedModules.put("spawner", new MobSpawnEdit());
		loadedModules.put("spawnerdata", new MobSpawnDataEdit());
		loadedModules.put("pigmenNerf", new PigmenSubwayNerf());
		loadedModules.put("incapCommand", new IncapCommand());
		loadedModules.put("giveLevels", new GiveLevels());
		loadedModules.put("SpazzTextNerf", new SpazzTextNerf());
		loadedModules.put("KitCommand", new KitCommand());
		loadedModules.put("NoteCommand", new NoteCommand());
		loadedModules.put("PlayerInfo", new PlayerInfo());
		loadedModules.put("Lightning", new LightningCommand());
		loadedModules.put("Unequip", new UnequipCommand());
		loadedModules.put("HideandShow", new HideandShowCommand());
		//loadedModules.put("Clear", new ClearCommand());
		loadedModules.put("inv", new InvCommand());
		loadedModules.put("ToggleFlyCommand", new ToggleFlyCommand());
		loadedModules.put("SavePlayers", new SavePlayersCommand());
		loadedModules.put("PlayerHeadCommand", new PlayerHeadCommand());
		loadedModules.put("BanCommand", new BanCommand());
		loadedModules.put("KickCommand", new KickCommand());
		loadedModules.put("BanTimeCommand", new BanTimeCommand());
		loadedModules.put("PardonCommand", new PardonCommand());
		loadedModules.put("FineCommand", new FineCommand());
		loadedModules.put("PluginCommand", new PluginCommmand());
		loadedModules.put("LockdownCommand", new LockdownCommand());
		loadedModules.put("EntitySpawnCommand", new EntitySpawnCommand());
		loadedModules.put("WorldTeleport", new WorldTp());
		loadedModules.put("NetherCalcCommand", new NetherCalcCommand());
		loadedModules.put("Minecart", new MinecartSpeed());
		
		for (IModule module : loadedModules.values()) {
			module.registerModule(this);
		}
	}
	
	private boolean setupEconomy() {
		final RegisteredServiceProvider<Economy> economyProvider = this.getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
		if (economyProvider != null) {
			economy = economyProvider.getProvider();
		}

		return (economy != null);
	}
	
	private boolean setupPermissions() {
		final RegisteredServiceProvider<Permission> permissionProvider = this.getServer().getServicesManager().getRegistration(net.milkbowl.vault.permission.Permission.class);
		if (permissionProvider != null) {
			permission = permissionProvider.getProvider();
		}
		return (permission != null);
	}

	
	public Economy getEconomy() {
		return economy;
	}
	
	public Permission getPermission() {
		return permission;
	}


}
