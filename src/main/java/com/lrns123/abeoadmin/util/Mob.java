package com.lrns123.abeoadmin.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.bukkit.entity.EntityType;

public enum Mob {
	CHICKEN("Chicken", Enemies.FRIENDLY, EntityType.CHICKEN),
	COW("Cow", Enemies.FRIENDLY, EntityType.COW),
	CREEPER("Creeper", Enemies.ENEMY, EntityType.CREEPER),
	GHAST("Ghast", Enemies.ENEMY, EntityType.GHAST),
	GIANT("Giant", Enemies.ENEMY, EntityType.GIANT),
	PIG("Pig", Enemies.FRIENDLY, EntityType.PIG),
	PIGZOMB("PigZombie", Enemies.NEUTRAL, EntityType.PIG_ZOMBIE),
	SHEEP("Sheep", Enemies.FRIENDLY, EntityType.SHEEP),
	SKELETON("Skeleton", Enemies.ENEMY, EntityType.SKELETON),
	SLIME("Slime", Enemies.ENEMY, EntityType.SLIME),
	SPIDER("Spider", Enemies.ENEMY, EntityType.SPIDER),
	SQUID("Squid", Enemies.FRIENDLY, EntityType.SQUID),
	ZOMBIE("Zombie", Enemies.ENEMY, EntityType.ZOMBIE),
	WOLF("Wolf", Enemies.NEUTRAL, EntityType.WOLF),
	CAVESPIDER("CaveSpider", Enemies.ENEMY, EntityType.CAVE_SPIDER),
	ENDERMAN("Enderman", Enemies.ENEMY, EntityType.ENDERMAN),
	SILVERFISH("Silverfish", Enemies.ENEMY, EntityType.SILVERFISH),
	ENDERDRAGON("EnderDragon", Enemies.ENEMY, EntityType.ENDER_DRAGON),
	VILLAGER("Villager", Enemies.FRIENDLY, EntityType.VILLAGER),
	BLAZE("Blaze", Enemies.ENEMY, EntityType.BLAZE),
	MUSHROOMCOW("MushroomCow", Enemies.FRIENDLY, EntityType.MUSHROOM_COW),
	MAGMACUBE("MagmaCube", Enemies.ENEMY, EntityType.MAGMA_CUBE),
	SNOWMAN("Snowman", Enemies.FRIENDLY, EntityType.SNOWMAN),
	OCELOT("Ocelot", Enemies.NEUTRAL, EntityType.OCELOT),
	WITCH("Witch", Enemies.ENEMY, EntityType.WITCH),
	WITHER("Wither", Enemies.ENEMY, EntityType.WITHER),
	IRONGOLEM("IronGolem", Enemies.NEUTRAL, EntityType.IRON_GOLEM),
	FALLINGSAND("FallingSand", Enemies.NEUTRAL, EntityType.FALLING_BLOCK),
	PRIMEDTNT("PrimedTNT", Enemies.NEUTRAL, EntityType.PRIMED_TNT),
	PLAYER("Player", Enemies.ENEMY, EntityType.PLAYER);

	private Mob(String n, Enemies en, EntityType type)
	{
		this.name = n;
		this.type = en;
		this.creatureType = type;
	}

	final public String name;
	final public Enemies type;
	final private EntityType creatureType;
	private static final Map<String, Mob> names = new HashMap<String, Mob>();
	private static final Map<EntityType, Mob> types = new HashMap<EntityType, Mob>();

	static
	{
		for (Mob mob : Mob.values())
		{
			names.put(mob.name.toLowerCase(), mob);
			types.put(mob.creatureType, mob);
		}
	}
	
	public static Set<String> getMobList() {
		return names.keySet();
	}

	public enum Enemies
	{
		FRIENDLY("friendly"),
		NEUTRAL("neutral"),
		ENEMY("enemy");

		private Enemies(final String type)
		{
			this.type = type;
		}
		final protected String type;
	}

	public EntityType getType()
	{
		return creatureType;
	}

	public static Mob fromName(final String name)
	{
		return names.get(name.toLowerCase());
	}
	
	public static Mob fromType(final EntityType type)
	{
		return types.get(type);
	}
}