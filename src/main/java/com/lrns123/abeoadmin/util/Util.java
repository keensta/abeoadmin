package com.lrns123.abeoadmin.util;


/**
 * 
 * @author Lourens Elzinga
 *
 */
public class Util {

	public static boolean isInteger(String str)
	{
		try {
			Integer.parseInt(str);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public static int toInteger(String str, int def)
	{
		try {
			return Integer.parseInt(str);
		} catch (Exception e) {
			return def;
		}
		
	}
}
