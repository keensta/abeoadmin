package com.lrns123.abeoadmin.runnables;

import com.lrns123.abeoadmin.modules.PlayerSilence;

public class SilenceExpireTask implements Runnable {

	private final PlayerSilence manager;
	private final String player;
	
	public SilenceExpireTask(PlayerSilence manager, String player){
		this.manager = manager;
		this.player = player;
	}
	
	@Override
	public void run(){
		manager.silenceExpire(player);
	}
}
