package com.lrns123.abeoadmin.runnables;

import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;

public class WorldSave implements Runnable{
	
	Logger log = Logger.getLogger("Minecraft");

	@Override
	public void run() {
		for (Player ply : Bukkit.getOnlinePlayers()) {
			ply.saveData();
		}
		log.info("Player data saved!");
		for (World world : Bukkit.getWorlds()) {
			  world.save();
		}
		log.info("Worlds saved!");
	}
	
	

}
